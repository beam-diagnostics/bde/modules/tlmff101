require tlmff101

epicsEnvSet("PREFIX",   "MFF101")

epicsEnvSet("SERIAL_PORT", "/dev/ttyUSB0")

# tlMFF101Config(port, vendorNum, productNum, serialNumberStr, priority, flags)
tlMFF101Config("$(PREFIX)", 0x0403, 0xFAF0, "37861633")
asynSetTraceIOMask("$(PREFIX)",0,0xff)
#asynSetTraceMask("$(PREFIX)",0,0xff)

# Load record instances
dbLoadRecords("tlMFF101.template","P=$(PREFIX):,R=,PORT=$(PREFIX),ADDR=0,TIMEOUT=1")
dbLoadRecords("asynRecord.db",    "P=$(PREFIX):,R=asyn,PORT=$(PREFIX),ADDR=0,OMAX=100,IMAX=100")
